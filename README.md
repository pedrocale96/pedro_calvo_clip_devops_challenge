# Pedro_Calvo_Clip_DevOps_challenge



## Hello, Clip Team!

First of all, thank you for the time and the opportunity given with this process and this interview!
Let's get started!

## 1) IaC

For the IaC part of the challenge I decided to use terraform to create the Infrastructure. The main.tf file contains the infrastructure that is being deployed, the datasources.tf contains tha data of the image that the EC2's are going to use and the providers.tf file contains the cloud provider info and the proper credentials in order to interact with AWS. Due that the database is inside a private subnet, I decided to make a modification to the given diagram and added another EC2 instance in a public subnet with an Internet Gateway, that works as a Bastion and allows me to access de Database. Also I used draw.io to recreate the diagram:

![](images/iac.png)

After running the terraform apply, here are the 2 working EC2 instances and the MySQL database instance:
![](images/aws1.png)
![](images/aws2.png)
## 2) Mysql scheme and table

Once that I have a way to access the database, I used MySQL Workbench in order to create a connection to the database and proceeded to create the scheme and the required table:
![](images/sql1.png)

In order to test the db, I added a couple of entries to the db with some of my personal pets that are no longer with us.
![](images/sql2.png)

## 3) App
For the App, I decided to use python since is the language that I'm most familiar with. All python code is locate on the app.py file. Because of the data is already created on the database and we only need to retrieve it, I focused on creating only the GET method which looks like this: 

```python
@app.route('/api/pet/<name>',methods=['GET'])
def get_task(name):
    task= Task.query.get(name)
    return task_schema.jsonify(task)

if __name__=="__main__":
    app.run(debug=True)
```

Once you make the api call, you should get the info on json format like this:
```json
{
    "name":"Bobby",
    "owner": "Kathy",
    "specie": "Pug",
    "sex": "m"
}
```

## 4) Security Upgrade
At this point our primarly EC2 instance it's still on a private subnet and updating for security purposes would require an internet connection, so, an approach that can solve this would be to implement a NAT Gateway so the instance can reach out the internet without being exposed to it.
I created the Nat Gateway inside the public subnet, in this way, all of the egress traffic from the private subnet would point to the nat gateway on the public subnet, and the Nat gateway would point to the internet gateway already in place.
![](images/aws3.png)


## Extra Points
1) In order to expose my app to the internet, I'm thinking about creating a snapshot of the current instance with the app inside it and launch it again in a public subnet(modifying all the necessary resources so it can comunicate with the db, of course), now inside the public subnet would be publicly reachable.

2) Having in consideration that now our app is public on the internet, talking about scaling it's only a matter of time. I'm proposing to have an autoscaling scheme based on the CPU usage of the instances. In order to monitor this  we would use Cloudwatch and create a couple of alarms based on the CPU Usage. If CPU usage >50% then launch another instance, if <50% then delete the instance, with a minimum number of 2 instances.
Having a minimum of 2 instances handling the app workload would require to implement an Elastic Load balancer to handle and distribute the workload. Adding all that the infra would look like this:
![](images/scaling.png)

3) In order for a developer to access the database he would need a couple of permits and files. 
First of all he would need the key pair that used to ssh the bastion. From my side I would need to create a user and a password for his use in the database and send them to him. Also he would need to retrieve a couple of values from the different aws resources so he would need an IAM user in aws with readonly permits. Retrieving the endpoint of the db, the private ip of the bastion and the public dns of the bastion, he would be able to access the database using MySQL workbench as well.


***


## NOTE
The .gitlab-ci.yml script it's just a high-level overview idea of how I imagine the pipeline would need to be structured and was added this way knowing that creating an actual functioning pipeline, involving all of the resources here created, would take a little bit longer to actually make it work . Thanks a lot for your consideration and I hope to hear form you very soon!