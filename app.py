from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow

app=Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI']='mysql+pymysql://user:user1234@terraform-20220810234158932400000001.cugh1ls5zbwc.us-east-1.rds.amazonaws.com/Clip_db'

app.config['SQLALCHEMY_TRACK_MODIFICATIONS']=False 

db= SQLAlchemy(app)
ma= Marshmallow(app)

class Task(db.Model):
    id= db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(20), unique=True)
    owner=db.Column(db.String(20), unique=True)
    specie=db.Column(db.String(20), unique=True)
    sex=db.Column(db.String(1), unique=True)

    def __init__(self, name, owner, specie, sex):
        self.name= name
        self.owner= owner
        self.specie= specie
        self.sex= sex

db.create_all()

class TaskSchema(ma.Schema):
    class Meta:
        fields= ('name', 'owner','specie','sex' )

task_schema= TaskSchema()


@app.route('/api/pet/<name>',methods=['GET'])
def get_task(name):
    task= Task.query.get(name)
    return task_schema.jsonify(task)


if __name__=="__main__":
    app.run(debug=True)
