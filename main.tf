//VPC

resource "aws_vpc" "clip_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    Name = "Clip_vpc"
  }
}

// SUBNETS

resource "aws_subnet" "clip_private_subnet1" {
  vpc_id                  = aws_vpc.clip_vpc.id
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "Clip_private_subnet_1"
  }
}
resource "aws_subnet" "clip_private_subnet2" {
  vpc_id                  = aws_vpc.clip_vpc.id
  cidr_block              = "10.0.2.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1b"

  tags = {
    Name = "Clip_private_subnet_2"
  }
}

resource "aws_subnet" "test_subnet1" {
  vpc_id                  = aws_vpc.clip_vpc.id
  cidr_block              = "10.0.3.0/24"
  map_public_ip_on_launch = true
  availability_zone       = "us-east-1a"

  tags = {
    Name = "public subnet"
  }
}

resource "aws_db_subnet_group" "clip_subnet_group" {
  name       = "clip_subnet_group"
  subnet_ids = [aws_subnet.clip_private_subnet2.id, aws_subnet.clip_private_subnet1.id]

  tags = {
    Name = "Clip_subnet_group"
  }
}

// INTERNET GATEWAY

resource "aws_internet_gateway" "test_igw" {
  vpc_id = aws_vpc.clip_vpc.id

  tags = {
    Name = "Test-IGW"
  }
}

//  SECURITY GROUPS

resource "aws_security_group" "clip_sg" {
  name        = "Clip_sg"
  description = "Security group created for Clip DevOps Challenge"
  vpc_id      = aws_vpc.clip_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "clip_db_sg" {
  name        = "Clip_db_sg"
  description = "Security group created for Clip DevOps Challenge"
  vpc_id      = aws_vpc.clip_vpc.id

  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["10.0.3.236/32", "10.0.1.215/32"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

//  KEY PAIR

resource "aws_key_pair" "clip_ssh_key_pair" {
  key_name   = "terra_key"
  public_key = file("~/.ssh/terra_key.pub")
}

// INSTANCES
resource "aws_instance" "clip_ec2" {
  ami                    = data.aws_ami.amazon-linux-2.id
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.clip_ssh_key_pair.id
  vpc_security_group_ids = [aws_security_group.clip_sg.id]
  subnet_id              = aws_subnet.clip_private_subnet1.id

  root_block_device {
    volume_size = 10
  }

  tags = {
    Name = "Clip_ec2_instance"
  }
}

resource "aws_db_instance" "Clip_rds" {
  allocated_storage      = 10
  engine                 = "mysql"
  engine_version         = "5.7"
  instance_class         = "db.t2.micro"
  name                   = "Clipdb"
  username               = "user"
  password               = "user1234"
  parameter_group_name   = "default.mysql5.7"
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.clip_subnet_group.id
  vpc_security_group_ids = [aws_security_group.clip_db_sg.id]
}

resource "aws_instance" "public_ec2" {
  ami                    = data.aws_ami.amazon-linux-2.id
  instance_type          = "t2.micro"
  key_name               = aws_key_pair.clip_ssh_key_pair.id
  vpc_security_group_ids = [aws_security_group.clip_sg.id]
  subnet_id              = aws_subnet.test_subnet1.id

  root_block_device {
    volume_size = 10
  }

  tags = {
    Name = "Bastion"
  }
}


// ROUTE TABLE ASSOCIATIONS

resource "aws_route_table" "test_public_rt" {
  vpc_id = aws_vpc.clip_vpc.id

  tags = {
    Name = "test_public_rt"
  }
}

resource "aws_route" "default_route" {
  route_table_id         = aws_route_table.test_public_rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.test_igw.id
}

resource "aws_route_table_association" "test_public_assoc" {
  subnet_id      = aws_subnet.test_subnet1.id
  route_table_id = aws_route_table.test_public_rt.id
}

resource "aws_route_table" "private_routetable" {
  vpc_id = aws_vpc.clip_vpc.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.clip_natgw.id
  }

  tags = {
    Name = "Private route table"
  }
}

resource "aws_route_table_association" "private_assoc" {
  subnet_id      = aws_subnet.clip_private_subnet1.id
  route_table_id = aws_route_table.private_routetable.id
}

// EIP

resource "aws_eip" "eip_for_natgw" {
  vpc = true
  tags = {
    Name = "EIP for NatGateway"
  }
}

//  NAT GATEWAY
resource "aws_nat_gateway" "clip_natgw" {
  allocation_id = aws_eip.eip_for_natgw.id
  subnet_id     = aws_subnet.test_subnet1.id

  tags = {
    Name = "Nat Gateway"
  }
  depends_on = [aws_internet_gateway.test_igw]
}